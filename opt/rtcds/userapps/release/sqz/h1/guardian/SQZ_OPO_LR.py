# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: SQZ_OPO_LR.py $
# Nutsinee Kijbunchoo Jan 29, 2019
# This guardian will lock the OPO using the original scheme
#

import sys
import time
from guardian import GuardState, GuardStateDecorator, NodeManager
import cdsutils as cdu
import numpy as np
import gpstime

import sqzparams

nominal = 'LOCKED_CLF_DUAL'
request = 'DOWN'

use_pump_iss = sqzparams.use_pump_iss

#############################################
#Functions
def OPO_locked():
    #sed edited this so that it doesn't need to read in a parameter that depends on pump power
    if ezca['SQZ-OPO_SERVO_COMEXCEN'] or ezca['SQZ-OPO_SERVO_FASTEXCEN']:
        notify('CMB EXC is ON')
    return ezca['SQZ-OPO_TRANS_DC_NORMALIZED'] > ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL']

def TTFSS_LOCKED():
    flag = False
    if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
        flag = True
    return flag

def in_softfault():
    if ezca['SQZ-OPO_PZT_1_RANGE'] != 0:
        notify('PZT voltage limits exceeded.')
        return True
    elif not OPO_locked():
        notify('OPO not really locked or maybe locked in a wrong mode')
        return True

def in_hardfault():
    if ezca['SQZ-LASER_IR_DC_POWERMON'] < 10:
        notify('SQZ LASER OFF??')
        return True
    if ezca['SQZ-LASER_IR_DC_ERROR_FLAG'] != 0:
        notify('Squeezer laser PD error')
        return False #True #vx 2/6

def EOM_OK():
    return abs(ezca['SQZ-FIBR_SERVO_EOMRMSMON']) < 4

def down_pump_iss():
    #turn off and reset opo pump iss
    ezca['SQZ-OPO_ISS_BOOST1'] = 0     #turn off pump iss boosts
    ezca['SQZ-OPO_ISS_BOOST2'] = 0
    ezca['SQZ-OPO_ISS_OUTPUTRAMP'] = 1 #turn off pump iss (off="ramp")
    #notify('disabled pump iss')
def engage_pump_iss():
    ezca['SQZ-OPO_ISS_OUTPUTRAMP'] = 0
    time.sleep(0.5)
    ezca['SQZ-OPO_ISS_BOOST1'] = 1     #turn on pump iss boosts
    ezca['SQZ-OPO_ISS_BOOST2'] = 1

def ISS_NOT_OK():
    if ezca['SQZ-OPO_ISS_LIMITCOUNT'] > 20:
       notify('No pump ISS means injecting bad squeezing. Going DOWN.')
       return True

def green_power_too_high():
    if ezca['SQZ-SHG_LAUNCH_DC_POWERMON']>35:
       notify('too much pump light into fiber? Adjust the half wave plate.')
       ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=0 #Added by NK March 25 2024. The point of this state is to protect the green fiber. Close the shutter to protect it.
       return True

'''
def read_params():
    if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] <= 17:
        params = sqzparams.pump_launch[17]
    elif ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] <= 14:
        params = sqzparams.pump_launch[14]
    elif ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] <= 9:   #VX 5/4 changed from 7->13, since 4/18 we're using 8mW
        params = sqzparams.pump_launch[7]
    return params
'''
#############################################
#Decorator
class softfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_softfault():
            return 'DOWN'

class hardfault_checker(GuardStateDecorator):
    def pre_exec(self):
        if in_hardfault():
            return 'DOWN' #'IDLE'

class EOM_checker(GuardStateDecorator):
    def pre_exec(self):
        if not EOM_OK():
            ezca['SQZ-FIBR_SERVO_EOMEN']=0
            return 'CHECK_EOM'

class GRLAUNCHchecker(GuardStateDecorator):
    def pre_exec(self):
        if green_power_too_high():
            return 'DOWN'

class ISS_checker(GuardStateDecorator):
    def pre_exec(self):
        if ISS_NOT_OK():
            return 'DOWN'

#############################################
#States

class INIT(GuardState):
    index = 0

    def main(self):
        return True

class DOWN(GuardState):
    index = 1
    goto = True

    @GRLAUNCHchecker
    def main(self):
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0
        ezca['SQZ-OPO_SERVO_IN2EN'] = 0
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0

        #turn off and reset opo pump iss
        down_pump_iss()
        ezca['SQZ-OPO_ISS_LIMITRESET'] = 1 #reset lockloss counter
        time.sleep(0.5)

        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] = 0

        #based on Daniel OPO TF measurements w/PZT1, alog 62741
        ezca['SQZ-OPO_SERVO_COMFILTER'] = 1 # common filter on
        ezca['SQZ-OPO_SERVO_SLOWCOMP'] = 0 # no compensation in slow path, maybe need if using Thorlabs PZT driver
        #ezca['SQZ-OPO_SERVO_IN1GAIN'] = -14 #NK March12 Let's SDF take care of this.

        ezca['SQZ-OPO_SERVO_SLOWOFSEN'] = 0 # OPO servo slowoffset to 0
        ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = 0

        # fully disable dither lock when not using
        ezca.get_LIGOFilter('SQZ-OPO_IR_LSC_SERVO').switch_off('FM1','FM2','INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-OPO_SERVO_EXC').switch_off('INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-OPO_IR_LSC_DITHER').switch_off('INPUT','OUTPUT')

    def run(self):
        return True

class IDLE(GuardState):
    index = 3
    request = False
    #goto = True

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
            notify('Scan timeout. Check trigger level.')

        notify('Cannot lock OPO. Check pump light on SQZT0.')
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #VX added 4/26
        ezca['SQZ-OPO_SERVO_COMBOOST'] = 0

        # 4/6 is this an effective way to protect the green pump fiber?
        return (ezca['SQZ-SHG_GR_DC_NORMALIZED'] > 0.2) and (ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] < 30) #20mW to protect to fiber


####################################################
# CLF LOCKING
####################################################
class PREP_LOCK_CLF(GuardState):
    index = 14
    request = False

    @ISS_checker
    def main(self):
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL'] = 1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1       #clf light through seed/clf fiber switch
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 1   #switch seed/clf to CLF
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0                #block seed from clf/seed fiber switch
        notify('CLF light in.')
        ezca['SYS-MOTION_C_SHUTTER_I_OPEN'] = 1   #open CLF trig shutter

        ezca['SQZ-OPO_SERVO_SLOWOFSEN'] = 0
        time.sleep(0.1)
        ezca['SQZ-OPO_PZT_1_SCAN_RESET'] = 1
        time.sleep(0.1)

        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 35 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 30:
            ezca['SQZ-OPO_TEC_SETTEMP'] = sqzparams.coresonance_temp

    def run(self):
        if (ezca['SQZ-FREQ_CLF']-3.125e6) >= 50e3:
            notify('CLF frequency out of range, check CLF CMB')
        elif ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 35 and ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 30:
            return True


class SCANNING_CLF_DUAL(GuardState):
    index = 15
    request = False

    def main(self):
        #turn off intensity servo and clear history before scanning

        #ezca.switch('SQZ-SHG_PWR_SERVO', 'INPUT' , 'OFF')
        #ezca['SQZ-SHG_PWR_SERVO_RSET']=2

        #Just send a message and continue. User should check whether if seed or CLF is being used (NK Aug28).
        if ezca['SYS-MOTION_C_SHUTTER_I_STATE'] == 1:
           notify('CLF TRIG shutter on SQZT7 closed.')

        #make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=60 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=2 # Set PZT scan trigger to 'And'.
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 0 # This looks at refl trans normalized AND 6MHz RF mon, 1 for lock seed dual

        # setting trigger levels
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = 0.5 #sqzparams.green_trigger_level
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = 0.5

        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True

        if ezca['SYS-MOTION_C_SHUTTER_I_STATE'] == 1:
           notify('CLF TRIG shutter on SQZT7 closed.')
        if not ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_READBACK'] == 1:
           notify('Check CLF/seed fiber switch. CLF should go in.')
        if not ezca['SQZ-CLF_FLIPPER_READBACK']:
           notify('CLF shutter closed. Check fiber switch before open. CLF should go in.')
        return False  #VX 8/24 - why does this return false?


def LOCKING():
    class LOCKING(GuardState):
        request = False

        def main(self):
            ezca['SQZ-OPO_SERVO_IN1EN'] = 1 #engage OPO locking
            time.sleep(0.5)

        def run(self):
            #check if OPO is locked fine. Then engage boosts
             # VX 4/22/22 - updated based on Daniel TFs, see alog 62741
            ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 1    # VX 4/28 - slow boost should always be on, but it gives -4.9V offset when initially locking, so turn slow boost on after closing loop to avoid this open-loop common offset
            time.sleep(0.5)
            ezca['SQZ-OPO_SERVO_COMBOOST']= 1
            time.sleep(1)
            if OPO_locked():
                return True
            else:
                notify('OPO not locked?? Going down')
                return 'DOWN'
                #return 'IDLE'
    return LOCKING


LOCKING_CLF_DUAL = LOCKING()
LOCKING_CLF_DUAL.index = 16



class LOCKED_CLF_DUAL_NO_ISS(GuardState):
    index = 17
    request = True
    def main(self):
        down_pump_iss()
        ezca['SQZ-OPO_ISS_LIMITRESET'] = 1 #reset lockloss counter
        log('disengaged pump iss')
        time.sleep(1)
    def run(self):
        #if ezca.read('GRD-SQZ_OPO_LR_REQUEST_S', as_string=True) == 'LOCKED_CLF_DUAL':
        return True

class ENGAGE_PUMP_ISS(GuardState):
    index = 18
    request = True

    def main(self):
        self.timer['pause'] = 0
        self.pump_engage_setpoint = 0.02
        self.counter = 0
        self.nominal_opo_trans = ezca['SQZ-OPO_TRANS_LF_OUTPUT']

    def run(self):
        if ezca['SQZ-OPO_ISS_LIMITCOUNT'] > 20:
            #return 'LOCKED_CLF_DUAL_NO_ISS' #commented by NK March 25 2024. It makes no sense to inject squeezing with no pump ISS. SQZ angle will drift. SQZ level will drift. Bad for IFO.
            notify('No pump ISS means injecting bad squeezing. Going DOWN.') #NK March 25 2024
            return 'DOWN' #NK March 25 2024
        if ezca['SQZ-SHG_LAUNCH_DC_POWERMON']>35:
            notify('too much pump light into fiber?')
            ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=0 #Added by NK March 25 2024. The point of this state is to protect the green fiber. If we don't like this power going into SHG fiber, we should close the flipper.
            return 'DOWN'

        if self.counter == 0: # go ahead if not using iss
            if not use_pump_iss:
                notify('not using OPO_ISS')
                return True
            elif self.timer['pause']:
                #opo_iss servo setpoint is based on opo_trans_volts/2
                #use the "unlocked" opo trans volts as the nominal setpoint
                if sqzparams.opo_iss_servoToSetpoint:
                    self.nominal_opo_trans = sqzparams.opo_grTrans_setpoint_uW
                else: #just servo to the current opo pump trans value
                    self.nominal_opo_trans = np.round(cdu.avg(1,'H1:SQZ-OPO_TRANS_LF_OUTPUT'),3)
                log('nominal opo trans is %f'%self.nominal_opo_trans)
                #if iss setpoint is too far, jump it close before servo-ing
                if abs(ezca['SQZ-OPO_ISS_SETPOINT']*2-ezca['SQZ-OPO_TRANS_DC_VOLTS'])>0.3:
                    ezca['SQZ-OPO_ISS_SETPOINT'] = -0.5*(ezca['SQZ-OPO_TRANS_DC_VOLTS']-0.12)
                self.counter += 1

        elif self.counter == 1: # first ramp servo setpoint to zero error monitor
            #get ramp sign
            if ezca['SQZ-OPO_ISS_ERRORMON']>self.pump_engage_setpoint: sign = 1
            else: sign = -1
            #then ramp
            if abs(ezca['SQZ-OPO_ISS_ERRORMON']-self.pump_engage_setpoint)>0.015:
                ezca['SQZ-OPO_ISS_SETPOINT']-=(sign*0.01)
                #time.sleep(0.1)
            else:
                #ezca['SQZ-OPO_ISS_OUTPUTRAMP'] = 0 #flip switch from ramp to servo
                engage_pump_iss()
                log('pump iss engaged')
                self.timer['pause']=1
                self.counter += 1

        elif self.counter == 2 and self.timer['pause']: #move iss servo setpoint to reach same no-iss opo_trans value
            sign = np.sign(ezca['SQZ-OPO_TRANS_LF_OUTPUT']-self.nominal_opo_trans)
            if abs(self.nominal_opo_trans - ezca['SQZ-OPO_TRANS_LF_OUTPUT'])>0.1: #convergence threshold for iss setpoint
                log('nominal opo trans is %f'%self.nominal_opo_trans)
                log('current opo trans is %f'%ezca['SQZ-OPO_TRANS_LF_OUTPUT'])
                ezca['SQZ-OPO_ISS_SETPOINT']+=(sign*1e-3)
                #time.sleep(0.1)
            else:
                self.counter += 1

        elif self.counter == 3:
            if ezca['SQZ-OPO_ISS_CONTROLMON']>0 and not ezca['SQZ-OPO_ISS_LIMITREACHED'] and not ezca['SQZ-OPO_ISS_OUTPUTRAMP']:
                log('pump iss setpoint servo-ed to nominal opo_trans')
            return True



class LOCKED_CLF_DUAL(GuardState):
    index = 20

    #@softfault_checker
    #@hardfault_checker
    def main(self):
        if OPO_locked():
            notify('OPO locked')

    @softfault_checker
    @hardfault_checker
    #@EOM_checker
    def run(self):
        #OPOTRANSave = cdu.avg(-1,'H1:SQZ-OPO_TRANS_LF_OUTPUT', stddev = True)
        #if OPOTRANSave[1] > 0.05:
        #    notify('OPO TRANS NOISY')

        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            log('OPO servo output railing')
            return 'DOWN' #'SCANNING_CLF_DUAL'

        if not OPO_locked():
            return 'DOWN' #'SCANNING_CLF_DUAL'

        if ezca['SQZ-OPO_ISS_LIMITCOUNT'] > 10:
            if ezca['SQZ-OPO_ISS_CONTROLMON']>0 and not ezca['SQZ-OPO_ISS_LIMITREACHED'] and not ezca['SQZ-OPO_ISS_OUTPUTRAMP']:
                #if iss is fine, reset counter
                ezca['SQZ-OPO_ISS_LIMITRESET'] = 1 #reset lockloss counter
            else: #turn off and reset opo pump iss
                notify('Disabling pump iss after 10 lockloss couter. Going to through LOCKED_CLF_DUAL_NO_ISS to turn on again.')
                return 'LOCKED_CLF_DUAL_NO_ISS'

        if ezca['SQZ-SHG_FIBR_REJECTED_DC_POWERMON'] > 0.3:
            notify('pump fiber rej power in ham7 high, nominal 35e-3, align fiber pol on sqzt0.')
        #elif abs(sqzparams.opo_grTrans_setpoint_uW - ezca['SQZ-OPO_TRANS_LF_OUTPUT'])>1:
        #    notify('opo trans pump power not at sqzparams setting')
        #    return 'LOCKED_CLF_DUAL_NO_ISS'

        return True



class CHECK_EOM(GuardState):
    index = 9
    goto = False #So that Guardian won't find the shortest path to LOCKED with this state
    request = False

    @softfault_checker #this will make OPO rescan if it's not locked
    def run(self):
        notify('EOM noisy or railed, disengaged.')
        log('EOM noisy or railed, disengaged.')

        if ezca['SQZ-FIBR_LOCK_STATUS_LOCKED'] == 1:
            ezca['SQZ-FIBR_SERVO_EOMEN'] = 1

        if (ezca['SQZ-FIBR_SERVO_EOMEN'] == 1) and EOM_OK(): #If manually engage EOM and it doesn't rail
            return 'LOCKED_CLF_DUAL'

        #if OPO_locked() and (ezca['PSL-FSS_TPD_DC_OUTPUT']>1.5): #If OPO locks and ref cav OK
        #    return 'LOCKED_ON_DUAL'
        #if ezca['SQZ-SHG_LAUNCH_DC_POWERMON'] >20:
        #    return 'IDLE'


####################################################
# SEED DUAL LOCKING - Locked on green plus seed co-resonance - For NLG Measurement
####################################################
class PREP_LOCK_SEED_DUAL(GuardState):
    index = 34
    request = False

    def main(self):
        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0

        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 100:
            notify('Seed power above limit of fiber switch, returning DOWN')
            return 'DOWN'
        elif not ezca['SYS-MOTION_C_BDIV_E_CLOSEDSWITCH']:
            notify('BDIV OPEN, are you sure?')
            return 'DOWN'
        else:
            ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1

        if ezca['SQZ-HD_FLIPPER_READBACK']:
            ezca['SQZ-HD_FLIPPER_CONTROL'] = 1
            time.sleep(0.5)
            ezca['SQZ-HD_FLIPPER_CONTROL'] = 0

    def run(self):
        if ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 30 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 35:
            notify('This OPO temperature is in range of high NLG')
        elif ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] > 45 or ezca['SQZ-OPO_TEC_THERMISTOR_TEMPERATURE'] < 50:
            notify('This OPO temperature is in range of low NLG')

        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 50:
            notify('Launching >50mW into seed fiber on sqzt0.')
        return True


class SCANNING_SEED_DUAL(GuardState):
    index = 35
    request = False


    def main(self):

        ezca['SQZ-OPO_PZT_1_SCAN_RESET']=1

        #make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD'] = 60 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT'] = 2 # Set PZT scan trigger to 'Ch1 & Ch2'.
        ezca['SQZ-OPO_IR_RESONANCE_CONDITION'] = 1 # This looks at refl trans and OPO IR.

        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = sqzparams.green_trigger_level
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_2_LEVEL'] = sqzparams.seed_low_red_trigger_level # SQZ-OPO_IR_DC_POWER channel

        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT
        time.sleep(0.25)


    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            return True
        return False

LOCKING_SEED_DUAL = LOCKING()
LOCKING_SEED_DUAL.index = 36

class LOCKED_SEED_NLG(GuardState):
    index = 37

    @softfault_checker
    @hardfault_checker

    def main(self):
        if ezca['SQZ-OPO_IR_RESONANCE_SEED_NORM'] > 0.1:
            notify('OPO locked with low SEED, please measure NLG.')
            return True


class TRANSITION_SEED_CLF(GuardState):
    #Swaps between LOCKED_SEED_NLG and LOCKED_CLF_DUAL without unlocking.
    index = 30
    request = False
    @softfault_checker
    @hardfault_checker

    def run(self):
        if ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] == 0:
            notify('Switching from injecting Seed beam to CLF beam.')
            ezca['SQZ-SEED_FLIPPER_CONTROL'] = 0
            ezca['SQZ-CLF_FLIPPER_CONTROL'] = 1
            ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL']=1
            return True
        if ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] == 1:
            if ezca['SYS-MOTION_C_BDIV_E_CLOSEDSWITCH'] == 1:
                notify('Switching from injecting CLF beam to Seed beam.')
                ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1
                ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0
                ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL']= 0
                return True
            else:
                notify('Beam diverter is open, returning to LOCKED_CLF_DUAL.')
                return 'LOCKED_CLF_DUAL'


################################################################

LOCKING_ON_ANY = LOCKING()
LOCKING_ON_ANY.index = 6

class SCANNING(GuardState):
    index = 4
    request = False

    def main(self):
        # VX 4/6/22- copied this section in from other scanning classes above
        # make sure loop is open and boosts are off
        ezca['SQZ-OPO_SERVO_IN1EN'] = 0      # open OPO loop (make sure it's unlocked)
        ezca['SQZ-OPO_SERVO_COMBOOST']= 0    # turn off boosts
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0  # turn off boosts
        down_pump_iss()   # OPO ISS loop off

        ezca['SQZ-OPO_PZT_1_SCAN_RESET'] = 1

        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=1
        ezca['SQZ-FIBR_SERVO_RAMPEN']=1 #open OPO loop (make sure it's unlocked)
        ezca['SQZ-FIBR_SERVO_EOMEN']=0 #turn off EOM    #VX 4/25 - don't understand this
        #ezca['SQZ-FIBR_LOCK_TEMPERATURECONTROLS_ON']=0 #stops laser temp feedback   #VX 4/28 commented out - this is already removed from SCANNING_SEED_DUAL state

        ezca['SQZ-OPO_PZT_1_SCAN_PERIOD']=60 #scan rate
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_INPUT']=0 #trigger on green trans (NK Aug 28)
        ezca['SQZ-OPO_PZT_1_SCAN_USETRIGGER']=1 #Use trigger level
        ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_CHANNEL_1_LEVEL'] = sqzparams.green_trigger_level

        ezca['SQZ-OPO_PZT_1_SCAN_ENABLE']=1 #scan PZT
        notify('Scanning OPO PZT')

    def run(self):
        if ezca['SQZ-OPO_PZT_1_SCAN_ENABLE'] == 0:
            if ezca['SQZ-OPO_PZT_1_SCAN_TRIGGER_TIMEOUTERROR'] != 0:
                notify('Scan timeout. Check trigger level.')
                log('Scan timeout')
                return 'IDLE'
            else:   #VX 4/25/22, added this else statement
                notify('SCANNING STATE, but not scanning!')
            return True
        return False


class LOCKED(GuardState):
    index = 10

    @softfault_checker
    @hardfault_checker
    def main(self):
        return True


    #@softfault_checker
    @hardfault_checker

    def run(self):
        OPOREFLave = cdu.avg(1,'H1:SQZ-OPO_REFL_DC_POWER', stddev = True)
        if OPOREFLave[1] > 0.005:
            log('OPO REFL NOISY')
        if abs(ezca['SQZ-OPO_SERVO_SLOWMON']) > 9:
            log('OPO output rails')
            return 'DOWN'
        if not OPO_locked(): #4/13 - check OPO lock status
            log('OPO unlocked: OPO GR trans normalized < PZT trigger level.')
            return 'SCANNING'
        if ezca['SQZ-SEED_PZT_SCAN_ENABLE'] == 1:
            notify('SEED PZT SCANNING')

        return True



####################################################
# VX 8/19 adding OPO_IR/SEED dither lock
# WJ updated the lock acquisition on 10/18/2022
####################################################
class PREP_LOCK_DITHER(GuardState):
    index = 134
    request = False

    def main(self):

        if not ezca['SYS-MOTION_C_BDIV_E_CLOSEDSWITCH']: #close BDIV
            notify('WARNING: Opening beam diverter with SEED LIGHT ON')
            #return 'DOWN'

        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 100:
            notify('Seed power > 100mW into fiber')

        ezca['SQZ-SHG_FIBR_FLIPPER_CONTROL']=0  #close green flipper
        ezca['SQZ-CLF_FLIPPER_CONTROL'] = 0     #close clf flipper
        ezca['SQZ-OPO_IR_SELECT_FIBERSWITCH_CONTROL'] = 0  #fiber switch to SEED
        ezca['SQZ-SEED_FLIPPER_CONTROL'] = 1    #open seed flipper

        ezca['SQZ-OPO_SERVO_COMBOOST']= 0    # turn off boosts
        ezca['SQZ-OPO_SERVO_SLOWBOOST'] = 0  # turn off boosts
        ezca['SQZ-OPO_SERVO_COMFILTER'] = 0 # common filter off, don't need it

        # initialize opo_pzt servo dither
        ezca['SQZ-OPO_IR_LSC_OSC_TRAMP'] = 0.5
        #ezca['SQZ-OPO_IR_LSC_OSC_CLKGAIN'] = 0.1   ## dither output amplitude # in-vac 0.1, in-air 1.0
        ezca['SQZ-OPO_SERVO_IN1EN'] = 'Off'
        ezca['SQZ-OPO_SERVO_IN2GAIN'] = 0
        ezca['SQZ-OPO_IR_LSC_SERVO_TRAMP'] = 0.3
        ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','FM2','OFF', 'FM10','ON')
        ezca.get_LIGOFilter('SQZ-OPO_IR_LSC_SERVO').switch_on('INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-OPO_SERVO_EXC').switch_on('INPUT','OUTPUT')
        ezca.get_LIGOFilter('SQZ-OPO_IR_LSC_DITHER').switch_on('INPUT','OUTPUT')

        # sweep OPO CMB SLOW OFFSET manually to find refl dip
        self.sweepmin = 0.6
        self.sweepmax = 4.6
        self.sweepstep_fast = 0.02
        self.sweepstep_slow = 0.01 # changed from 0.01 CMC 10/17/2022
        #vx11/23 self.sweepstep_veryslow = 0.005

        self.maxIR = -100
        self.minIR = 100

        self.counter = 0

        ezca['SQZ-OPO_PZT_1_SCAN_RESET'] = 1  # reset PZT beckoff scan
        ezca['SQZ-OPO_SERVO_SLOWOFSEN']  = 0  # use OPO CMB slow offset for IR dither lock
        ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] = self.sweepmin  # set cmb offset to min before enabling
        time.sleep(0.5)
        ezca['SQZ-OPO_SERVO_SLOWOFSEN']  = 1  # use OPO CMB slow offset for IR dither lock
        if not ezca['SQZ-OPO_SERVO_COMEXCEN']:
            notify('OPO CMB EXC_A CLOSED to PZT dither, enabling EXC A')
            ezca['SQZ-OPO_SERVO_COMEXCEN'] = 1
        time.sleep(1)

        self.startgps = gpstime.gpsnow()
        log('Start gps = %f'%self.startgps)

    def run(self):  ## "PRESCAN_SEED" to get refl dip depth
        if not ezca['SQZ-OPO_SERVO_COMEXCEN']:
            notify('OPO EXC_A CLOSED to PZT dither')

        if ezca['SQZ-SEED_LAUNCH_DC_POWERMON'] > 80:
            notify('Seed power >80mW into fiber')

        if self.counter == 0:   # step OPO CMB SLOW OFFSET to sweep OPO PZT
            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += self.sweepstep_fast
            time.sleep(0.1)
            if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] > self.sweepmax:
                self.endgps = gpstime.gpsnow()
                log('End gps = %f'%self.endgps)
                self.counter += 1

        # ramp PZT up to find IR REFL dip
        elif self.counter == 1:
            notify('fetching REFL data using SHUTTER_I')
            _data = cdu.getdata(['H1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ'],self.endgps-self.startgps,self.startgps)
            _IRrefl = _data[0].data
            log('len(_IRrefl) = %f'%len(_IRrefl))

            self.sweepmax = ezca['SQZ-OPO_SERVO_SLOWOUTOFS']

            self.maxIR = max(_IRrefl)
            #self.minIR = min(_IRrefl)
            val, idx = min((val, idx) for (idx, val) in enumerate(_IRrefl))
            log('idx = %f'%idx)
            self.minIR = val

            #vx11/23 self.offsetmin = (idx/len(_IRrefl))*(self.sweepmax-self.sweepmin)+self.sweepmin
            #vx11/23 self.offsetmin = self.offsetmin - 1.5 # compensate for hysterisis

            log('max IR is %f'%self.maxIR)
            log('min IR is %f'%self.minIR)
            #vx11/23 log('min IR happened at SLOWOFS = %f'%self.offsetmin)
            self.ir_refl_threshold_trig = (self.maxIR-self.minIR)*0.5 + self.minIR
            log('threshold is %f'%self.ir_refl_threshold_trig)
            time.sleep(1.0)
            notify('Prescan has been done :)')  # completed UP PZT scan to find IR refl dip

            self.counter += 1

        elif self.counter == 2:

            # ramp PZT down to find IR REFL dip && don't ramp PZT voltage too low :
            while ezca['SQZ-SHUTTER_I_TRIGGER_OUT16'] > self.ir_refl_threshold_trig:
                ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] -= self.sweepstep_slow
                time.sleep(0.1)

                if ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] < 0.5:
                    notify('OPO PZT Scan exceeds range.')
                    return 'DOWN'

            ezca['SQZ-OPO_SERVO_SLOWOUTOFS'] += 0.08 # hysteresis

            log('Found refl dip')
            notify('Found refl dip, engaging servo.')

            self.counter += 1

        elif self.counter == 3:
            return True


class LOCKING_SEED_DITHER(GuardState):
    index = 135
    request = False

    def main(self):
        self.count = 0
        self.timeout = 60
        self.timer['timeout'] = self.timeout

    def run(self):
        if self.count == 0:
            if self.timer['timeout']:
                notify('Timed out. Going to prescan state.')
                return 'PREP_LOCK_DITHER'

            # if refl signal is below the threshold, turn on servo.
            ezca['SQZ-OPO_IR_LSC_SERVO_GAIN'] = -10   # set dither LSC servo gain
            time.sleep(0.5)
            ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM1','ON')  #FM1 = 0.1/10 boost
            time.sleep(0.5)
            ezca.switch('SQZ-OPO_IR_LSC_SERVO','FM2','ON')  #FM2 = 0/0.1 integrator
            notify('turned on integrator and boosts')
            self.timer['pause'] = 15
            self.count += 1

        elif self.count == 1 and self.timer['pause']:
            return True

class LOCKED_SEED_DITHER(GuardState):
    index = 136

    def main(self):
        self.ir_locked_refl_min = np.round(cdu.avg(1,'H1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ')*1.025)
        log('Setting lockloss threshold @ %f'%self.ir_locked_refl_min)

    def run(self):
        if ezca['SQZ-SEED_PZT_SCAN_ENABLE'] == 1:
            notify('SEED PZT SCANNING')
        #check OPO locking
        #OPOIRave = cdu.avg(1,'H1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ', stddev = True)
        #if OPOIRave[1] > 0.005:
        #    notify('OPO_IR noisy')

        opo_ir_refl = ezca['SQZ-SHUTTER_I_TRIGGER_OUT16']
        #opo_ir_refl = cdu.avg(0.5,'H1:SQZ-SHUTTER_I_TRIGGER_OUT_DQ')
        #%log(f'measured {opo_ir_refl=}, {self.ir_locked_refl_min=}')
        if opo_ir_refl > self.ir_locked_refl_min:  #check opo lock based on IR refl
            log(f'lockloss detected: {opo_ir_refl=} above lockloss threshold {self.ir_locked_refl_min}')
            #log('lockloss detected: IR_refl fell below threshold @ %f'%self.ir_locked_refl_min)
            notify('lockloss detected')
            return 'PREP_LOCK_DITHER'

        if not ezca['SQZ-OPO_SERVO_COMEXCEN']:
            notify('OPO EXC_A CLOSED to PZT dither')

        return True



##################################################

edges = [
    ('INIT', 'DOWN'),

    # LOCK CLF
    ('DOWN', 'PREP_LOCK_CLF'),
    ('PREP_LOCK_CLF', 'SCANNING_CLF_DUAL'),
    ('SCANNING_CLF_DUAL', 'LOCKING_CLF_DUAL'),
    #('LOCKING_CLF_DUAL', 'ENGAGE_PUMP_ISS'),  #vx 3/1
    ('LOCKING_CLF_DUAL', 'LOCKED_CLF_DUAL_NO_ISS'),
    ('LOCKED_CLF_DUAL_NO_ISS', 'ENGAGE_PUMP_ISS'),
    ('ENGAGE_PUMP_ISS', 'LOCKED_CLF_DUAL'),
    ('LOCKED_CLF_DUAL', 'LOCKED_CLF_DUAL_NO_ISS'),
    #('IDLE', 'SCANNING_CLF_DUAL'),   #vx 3/1
    ('CHECK_EOM', 'LOCKED_CLF_DUAL'),
    ('LOCKED_CLF_DUAL','TRANSITION_SEED_CLF'),
    ('TRANSITION_SEED_CLF', 'LOCKED_SEED_NLG'),

    ## LOCK SEED LOW/DUAL (TO MEASURE NLG ON SEED)
    #('DOWN', 'PREP_LOCK_SEED_DUAL'),
    #('PREP_LOCK_SEED_DUAL', 'SCANNING_SEED_DUAL'),
    #('SCANNING_SEED_DUAL', 'LOCKING_SEED_DUAL'),
    #('LOCKING_SEED_DUAL', 'LOCKED_SEED_NLG') ,
    ('LOCKED_SEED_NLG','TRANSITION_SEED_CLF'),
    ('TRANSITION_SEED_CLF', 'LOCKED_CLF_DUAL'),

    # LOCK SEED LOW/DUAL (TO MEASURE NLG ON SEED)
    ('LOCKED_CLF_DUAL','TRANSITION_SEED_CLF'),
    ('TRANSITION_SEED_CLF', 'LOCKED_SEED_NLG'),


    # DITHER LOCK OPO ON IR  (NO NLG)
    ('DOWN','PREP_LOCK_DITHER'),
    ('PREP_LOCK_DITHER','LOCKING_SEED_DITHER'),
    ('LOCKING_SEED_DITHER','LOCKED_SEED_DITHER'),
    #('IDLE', 'PREP_LOCK_DITHER'),     #vx 3/1

    # LOCK ANY
    ('DOWN', 'SCANNING'),
    #('LOCKED','DOWN'),
    #('IDLE', 'SCANNING'),  #vx 3/1
    ('SCANNING', 'LOCKING_ON_ANY'),
    ('LOCKING_ON_ANY', 'LOCKED')]

