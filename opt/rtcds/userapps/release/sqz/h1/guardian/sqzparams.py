########################################################
####### TYPE OF SQZ ####################################
########################################################
sqz_with_fds = True # Nominal is True fopr FDS, if there is issues with FC, change to False to use FIS


########################################################
####### FOR OPO_GRD ####################################
########################################################
SQZ_ANGLE = 146 # used as final sqz angle in THERMALIZATION guardian


########################################################
####### FOR OPO_GRD ####################################
########################################################

##############  pump power & ISS settings  ##############
opo_grTrans_setpoint_uW = 80  #reload OPO guardian for this to take effect
use_pump_iss = True
opo_iss_servoToSetpoint = True
use_clf_iss = True


########################################################
####### FOR FC GRD  ####################################
########################################################

############## fc locking params ##############
fcgs_trans_lock_threshold = 60   #was 80 #threshold 110 for 5mW FC launch is OK, can scale accordingly.
fcWFS_qDip_lock_threshold = 0
fc_wfs_a_ir_gain = -0.86 #for -13dBm RF6  #-2.6 for -23dBm RF6  #-0.13 for 6 dBm RF6  # updated higher clf O4b 2024Mar8  #-3.5  # FC IR gain
fc_cmb_in1_gain = 5
fc_green_sus_gain = 0.5 #FC GR sus gain


#######################################################
####### SQZ ASC CONFIGURATIONS ########################
#######################################################

##############  status of ASC's  ############## 
use_ifo_as42_asc = True

use_fc_asc = True
use_fc_beamspot_control = True


########################################################
######### OTHER  #######################################
########################################################

#use ADJUST_SQZ_ANG_ADF of SQZ_ANG_ADJUST to have sqz ange adjusted during FDS
use_sqz_angle_adjust = True   # If changed to false, edit the nominal state in SQZ_ANG_ADJUST to 'DOWN' 
# AND edit the nominal state in h1/sqz/scripts/switch_nom_sqz_states.py 

#values in use, pre-O4
coresonance_temp = 31.72
no_nlg_temp = 46.62      ## haven't checked this in a while
green_trigger_level = 0.5
opo_servo_in1gain = -14


#values to use for lock seed dual (0.5mW seed launch)
seed_low_red_trigger_level = 0.5


##############  common-mode servo gains  ############## 
''' updated 3/14
shg_servo_in1_gain = 7   #H1:SQZ-SHG_SERVO_IN1GAIN = 7, comm offset = -1.5, comm boost = 1, slow offset = 4.5, slowboost ON 
opo_servo_in1gain = -14  #H1:SQZ-OPO_SERVO_IN1GAIN = -14, comm offset = -0.1, comm boost = 1, slow offset = 0, slowboost ON 
clf_servo_in2_gain = 7   #H1:SQZ-CLF_SERVO_IN2GAIN = 7, comm = 0, comm boost = 2. only comm used for fb.
lo_servo_in1_gain = 3 (-sign)  #OMC_TRANS  #H1:SQZ-LO_SERVO_IN1GAIN = 3, comm offset = -1.15, comm boost = 2, not using slow path??
lo_servo_in2_gain = 7    #homodyne LO locking = 7. homodyne PZT locking = 31.  
fc_servo_in1_gain = 5   #H1:SQZ-FC_SERVO_IN1GAIN = 5, no boosts in CMB.

'''


'''
#parameters that depend on pump power --- OPO_GRD does not use this dictionary
pump_launch ={17:{green_trigger_level:0.5,#used for 17mW and higher
                  coresonance_temp:32.3,
                  no_nlg_temp:51.066,
                  opo_servo_in1gain: -17},
             14:{green_trigger_level:0.5, #used for 14mW and higher
                  coresonance_temp:32.3,
                  no_nlg_temp:46.619,
                  opo_servo_in1gain: -11},
             7:{green_trigger_level:0.5,      
                  coresonance_temp:32.3,    
                  no_nlg_temp:46.619,        
                  opo_servo_in1gain: -11},   
             3:{green_trigger_level:0.5,  
                  coresonance_temp:32.3,     
                  no_nlg_temp:46.619,  
                  opo_servo_in1gain: 0},     
            }
'''


# below is from O3
'''#values to use with nominal green power (18-20 mW into fiber)
coresonance_temp = 33.304
no_nlg_temp = 51.076
green_trigger_level = 0.5
opo_servo_in1gain = -17

#values to use for 14 mW into fiber
coresonance_temp = 33.288
no_nlg_temp = 51.066
green_trigger_level = 0.5
opo_servo_in1gain = -17
'''
